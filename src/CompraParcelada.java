
public class CompraParcelada extends Compra {

	private double jurosMensal = 0;
	private int parcelas = 0;

	public CompraParcelada(double valorCompra, int parcelas, double jurosMensal) {
		super(valorCompra);
		this.jurosMensal = jurosMensal;
		this.parcelas = parcelas;
	}

	@Override
	public double total() {
		super.valorTotal = super.valorTotal * Math.pow((1 + jurosMensal), parcelas);
		return super.total();
	}

}
