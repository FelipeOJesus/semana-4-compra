
public class Compra {
	
	protected double valorTotal = 0;

	public Compra(double valorCompra) {
		valorTotal += valorCompra;
	}
	
	public double total() {
		return valorTotal;
	}
}
