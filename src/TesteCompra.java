import static org.junit.Assert.*;

import org.junit.Test;

public class TesteCompra {

	@Test
	public void jurosCompostoUmaParcela() {
		CompraParcelada compraParcelada = new CompraParcelada(10000, 1, 0.10);
		assertEquals(11000, compraParcelada.total(), 0.01);
	}
	
	@Test
	public void jurosCompostoDuasParcelas() {
		CompraParcelada compraParcelada = new CompraParcelada(10000, 2, 0.10);
		assertEquals(12100, compraParcelada.total(), 0.01);
	}
	
	@Test
	public void jurosCompostoCincoParcelas() {
		CompraParcelada compraParcelada = new CompraParcelada(10000, 5, 0.10);
		assertEquals(16105.10, compraParcelada.total(), 0.01);
	}
	
	@Test
	public void jurosCompostoDezParcelas() {
		CompraParcelada compraParcelada = new CompraParcelada(10000, 10, 0.10);
		assertEquals(25937.42, compraParcelada.total(), 0.01);
	}

}
